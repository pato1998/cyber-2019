<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="assets/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/owl.theme.default.min.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,900&display=swap" rel="stylesheet">
    <title>Cyber Monday ShopUnilever</title>


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-116488191-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-116488191-4');
    </script>

  </head>
  <body>
    <!---preheader--->
    <section class="navbar-shop">
        <nav class="navbar navbar-light bg-light ">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <img src="img/logo_new.svg" width="150" alt="">
                </a>
            </div>       
        </nav>
    </section>

    <!---bannerinicial--->
    <section class="banner-inicial">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 pt-3 mb-3">
                    <img class="img-fluid"  src="img/logo_cyber.png" width="161" alt="cyber logo">
                    <h4 style="color: #234695;"><b>NO TE LO PIERDAS</b></h4>
                    <h1>Cyber Monday</h1>
                    <p class="parrafos"><b>4, 5 y 6 de Noviembre.<br/>Un solo botón. Todas las ofertas</b></p>
                    <a class="button_cyber mt-3" href="#">VER OFERTAS</a>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 pt-5">
                    <img class="img-fluid" src="img/productos.png" alt="productos cyber">
                </div>
            </div>
            <div class="col-12 py-5">
                <h1 class="text-center" style="color: #FFFFFF;">Ofertas Imperdibles</h1>
                <p class="parrafos text-center pb-5"><b>Aprovechá estos descuentos que tenemos para vos<br/>en las mejores marcas.</b></p>
            </div>
        </div>
    </section>

    <!-- banner principal -->
    <section class="container-fluid secciones">
        <div class="container">
            <l class="row">
                <ul class="nav" id="myTab" role="tablist">
                    <li class="col-3 p-0">
                        <a class="nav-link active" id="alimentos-tab" data-toggle="tab" href="#alimentos" role="tab" aria-controls="alimentos" aria-selected="true">
                            <div class="contenedor imghoover">
                                <img class="firstimg" src="img/alimentos-bebidas.png" alt="">     
                                <img class="secondimg dnone" src="img/alimentos-bebidas-hover.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li class="col-3 p-0 mt-5 mx-auto text-center">
                        <a class="nav-link" id="personal-tab" data-toggle="tab" href="#personal" role="tab" aria-controls="personal" aria-selected="false">
                            <div class="imghoover">
                                <img class="firstimg img-fluid imgfix" src="img/cuidado-personal.png" alt="">
                                <img class="secondimg dnone imgfix" src="img/cuidado-personal-hover.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li class="col-3 p-0 mt-5 mx-auto text-center">
                        <a class="nav-link" id="hogar-tab" data-toggle="tab" href="#hogar" role="tab" aria-controls="hogar" aria-selected="false">
                            <div class="imghoover">
                                <img class="firstimg img-fluid imgfix" src="img/cuidado-hogar.png" alt="">
                                <img class="secondimg dnone imgfix" src="img/cuidado-hogar-hover.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li class="col-3 p-0 mt-5 mx-auto text-center">
                        <a class="nav-link" id="ropa-tab" data-toggle="tab" href="#ropa" role="tab" aria-controls="ropa" aria-selected="false">
                            <div class="imghoover">
                                <img class="firstimg img-fluid imgfix" src="img/cuidado-ropa.png" alt="">
                                <img class="secondimg dnone imgfix" src="img/cuidado-ropa-hover.png" alt="">
                            </div>
                        </a>
                    </li>
                    <!--
                    <li class="col-3 p-0 imghooverdos mt-5">
                        <a class="nav-link" id="hogar-tab" data-toggle="tab" href="#hogar" role="tab" aria-controls="hogar" aria-selected="false">
                            <div class="imghooverdos">
                                <img class="firstimg imgfix" src="img/cuidado-hogar.png" alt="">
                                <img class="secondimg imgfix" src="img/cuidado-hogar-hover.png" alt="">
                            </div>
                        </a>
                    </li>
                    <li class="col-3 p-0 imghooverdos mt-5">
                        <a class="nav-link" id="ropa-tab" data-toggle="tab" href="#ropa" role="tab" aria-controls="ropa" aria-selected="false">
                            <div class="imghooverdos">
                                <img class="firstimg imgfix" src="img/cuidado-ropa.png" alt="">
                                <img class="secondimg dnone imgfix" src="img/cuidado-ropa-hover.png" alt="">
                            </div>
                        </a>
                    </li>-->
                </ul>
            </div>
        </div>
        <!-- Carousel -->
        <div class="container-fluid">
            <div class="tab-content row blog mt-3" id="myTabContent">
                    <!-- 
                        <div id="blogCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="row">
                                        <div class="col-md-3 text-center">
                                            <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                            <a class="button_cyber" href="#">COMPRAR</a>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                            <a class="button_cyber" href="#">COMPRAR</a>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                            <a class="button_cyber" href="#">COMPRAR</a>
                                        </div>
                                        <div class="col-md-3 text-center">
                                            <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                            <a class="button_cyber" href="#">COMPRAR</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="row">
                                        <div class="col-md-3 text-center">
                                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                                <a class="button_cyber" href="#">COMPRAR</a>
                                        </div>
                                        <div class="col-md-3 text-center">
                                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                                <a class="button_cyber" href="#">COMPRAR</a>
                                        </div>
                                        <div class="col-md-3 text-center">
                                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                                <a class="button_cyber" href="#">COMPRAR</a>
                                        </div>
                                        <div class="col-md-3 text-center">
                                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                                <a class="button_cyber" href="#">COMPRAR</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    <!-- desktop -->
                    <!-- 
                   <div class="row d-xl-flex d-lg-flex d-md-none d-sm-none d-xs-none">
                        <div class="col-1">
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" style="margin-left: 100px;margin-top: -30px;"  role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        </div>      
                        <div id="carouselExampleIndicators" class="carousel slide col-10" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                <a class="button_cyber" href="#">COMPRAR</a>
                                </div>
                                <div class="col-md-3 text-center">
                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                <a class="button_cyber" href="#">COMPRAR</a>
                                </div>
                                <div class="col-md-3 text-center">
                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                <a class="button_cyber" href="#">COMPRAR</a>
                                </div>
                                <div class="col-md-3 text-center">
                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                <a class="button_cyber" href="#">COMPRAR</a>
                                </div>
                            </div>
                            </div>
                            <div class="carousel-item">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                <a class="button_cyber" href="#">COMPRAR</a>
                                </div>
                                <div class="col-md-3 text-center">
                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                <a class="button_cyber" href="#">COMPRAR</a>
                                </div>
                                <div class="col-md-3 text-center">
                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                <a class="button_cyber" href="#">COMPRAR</a>
                                </div>
                                <div class="col-md-3 text-center">
                                <img src="img/Unilever-CM2019_10.png" alt="Image" style="max-width:100%;">
                                <a class="button_cyber" href="#">COMPRAR</a>
                                </div>
                            </div>
                            </div>
                        </div>

                        <ol class="carousel-indicators-style carousel-indicators ">
                            <li data-target="#carouselExampleIndicators" style="height: 15px; width: 15px;" data-slide-to="0" class="active mx-1"></li>
                            <li data-target="#carouselExampleIndicators" style="height: 15px; width: 15px;" data-slide-to="1" class="mx-1"></li>
                        </ol>


                        </div>
                        <div class="col-1">
                        <a class="carousel-control-next " href="#carouselExampleIndicators" style="margin-right: 100px;margin-top: -30px;" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                        </div>
                  </div> -->
                <!-- Alimentos -->
                <div class="tab-pane fade show active container-fluid mx-auto mt-5" id="alimentos" role="tabpanel" aria-labelledby="alimentos-tab" style="width: 80%;">
                    <div class="owl-carousel owl-theme">
                    <div class="item">
<img src="img/productos/alimentos/savora-most-orig-spacia.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='alimentos' data-product='SAVORA MOST ORIG SPACIA' data-source='savora-most-orig-spacia'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/alimentos/maizena-premezcla-bizco-vainilla.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='alimentos' data-product='MAIZENA PREMEZCLA BIZCO VAINILLA' data-source='maizena-premezcla-bizco-vainilla'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/alimentos/hellmanns-may-light-stacc.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='alimentos' data-product='HELLMANNS MAY LIGHT S/TACC' data-source='hellmanns-may-light-s/tacc'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/alimentos/hellmanns-may-aceite-palta.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='alimentos' data-product='HELLMANNS MAY ACEITE PALTA' data-source='hellmanns-may-aceite-palta'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/alimentos/maizena-premezcla-bizco-vainilla.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='alimentos' data-product='MAIZENA PREMEZCLA BIZCO VAINILLA' data-source='maizena-premezcla-bizco-vainilla'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/alimentos/maizena-premezcla-brownie.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='alimentos' data-product='MAIZENA PREMEZCLA BROWNIE' data-source='maizena-premezcla-brownie'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/alimentos/lipton-infusion-menta.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='alimentos' data-product='LIPTON INFUSION MENTA' data-source='lipton-infusion-menta'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/alimentos/lipton-te-yellow-label.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='alimentos' data-product='LIPTON TE YELLOW LABEL' data-source='lipton-te-yellow-label'>COMPRAR</a>
</div>

                    </div>
                </div>

                <!-- Cuidado personal -->
                <div class="tab-pane fade show container-fluid mx-auto mt-5" id="personal" role="tabpanel" aria-labelledby="personal-tab" style="width: 80%;">
                    <div class="owl-carousel owl-theme">
     
<div class="item">
<img src="img/productos/personal/dove-jab-blanco.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='personal' data-product='DOVE JAB BLANCO' data-source='dove-jab-blanco'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/personal/dove-ac-reconstruccion-completa.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='personal' data-product='DOVE AC RECONSTRUCCION COMPLETA' data-source='dove-ac-reconstruccion-completa'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/personal/close-up-c-d-eucalyptus-freeze.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='personal' data-product='CLOSE UP C D EUCALYPTUS FREEZE' data-source='close-up-c-d-eucalyptus-freeze'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/personal/rexona-jab-glicerina-citrus.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='personal' data-product='REXONA JAB GLICERINA CITRUS' data-source='rexona-jab-glicerina-citrus'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/personal/rexona-jab-glicerina-neutro.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='personal' data-product='REXONA JAB GLICERINA NEUTRO' data-source='rexona-jab-glicerina-neutro'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/personal/lux-hw-orquidea-negra.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='personal' data-product='LUX HW ORQUIDEA NEGRA' data-source='lux-hw-orquidea-negra'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/personal/rexona-jab-liq-antib-aloe.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='personal' data-product='REXONA JAB LIQ ANTIB ALOE' data-source='rexona-jab-liq-antib-aloe'>COMPRAR</a>
</div>

                    </div>
                </div>

                <!-- Cuidado Ropa -->
                <div class="tab-pane fade show container-fluid mx-auto mt-5" id="ropa" role="tabpanel" aria-labelledby="ropa-tab" style="width: 80%;">
                    <div class="owl-carousel owl-theme">
                    <div class="item">
<img src="img/productos/ropa/skip-liq-lv-ropa-regular.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='ropa' data-product='SKIP LIQ LV ROPA REGULAR' data-source='skip-liq-lv-ropa-regular'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/ropa/vivere-regular-clasico.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='ropa' data-product='VIVERE REGULAR CLASICO' data-source='vivere-regular-clasico'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/ropa/ala-liq-plv-ropa-baja-espuma.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='ropa' data-product='ALA LIQ P/LV ROPA BAJA ESPUMA' data-source='ala-liq-p/lv-ropa-baja-espuma'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/ropa/ala-multiac-liq-doyp.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='ropa' data-product='ALA MULTIAC LIQ DOYP' data-source='ala-multiac-liq-doyp'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/ropa/vivere-ref-viol-y-flores.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='ropa' data-product='VIVERE REF VIOL Y FLORES' data-source='vivere-ref-viol-y-flores'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/ropa/comfort-conc-original.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='ropa' data-product='COMFORT CONC ORIGINAL' data-source='comfort-conc-original'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/ropa/skip-liq-lv-ropa-recular.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='ropa' data-product='SKIP LIQ LV ROPA RECULAR' data-source='skip-liq-lv-ropa-recular'>COMPRAR</a>
</div>
                    </div>
                </div>

                <!-- Cuidado Hogar -->
                <div class="tab-pane fade show container-fluid mx-auto mt-5" id="hogar" role="tabpanel" aria-labelledby="hogar-tab" style="width: 80%;">
                    <div class="owl-carousel owl-theme">
                       
<div class="item">
<img src="img/productos/hogar/cif-cr-limon-c/microp.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='hogar' data-product='CIF CR LIMON C/MICROP' data-source='cif-cr-limon-c/microp'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/hogar/cif-vidrios-rep-econ-doyp.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='hogar' data-product='CIF VIDRIOS REP ECON DOYP' data-source='cif-vidrios-rep-econ-doyp'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/hogar/cif-antig-ultra-rapido.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='hogar' data-product='CIF ANTIG ULTRA RAPIDO' data-source='cif-antig-ultra-rapido'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/hogar/cif-bano-rep-econ.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='hogar' data-product='CIF BANO REP ECON' data-source='cif-bano-rep-econ'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/hogar/vim-lvdina-gel-original.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='hogar' data-product='VIM LVDINA GEL ORIGINAL' data-source='vim-lvdina-gel-original'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/hogar/sun-polvo-lv-vaj-progress.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='hogar' data-product='SUN POLVO LV VAJ PROGRESS' data-source='sun-polvo-lv-vaj-progress'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/hogar/cif-active-gel-core-limon.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='hogar' data-product='CIF ACTIVE GEL CORE LIMON' data-source='cif-active-gel-core-limon'>COMPRAR</a>
</div>

<div class="item">
<img src="img/productos/hogar/cif-active-gel-core-limon-verde.jpg" alt="Image" class="mb-3 margenimg" style="width:60%;">
<a class='button_cyber open-modal' data-category='hogar' data-product='CIF ACTIVE GEL CORE LIMON VERDE' data-source='cif-active-gel-core-limon-verde'>COMPRAR</a>
</div>

                    </div>
                </div>


            </div>
        </div>
                  <!-- mobile  --> 
    </section>

    <!---form--->
    <section class="formulario container-fluid mt-5 py-5">
        <div class="container pt-5">
            <h1 class="text-center" style="color: #FFFFFF;">No te pierdas las últimas novedades</h1>
            <p class="text-center" style="color: #00b8db; font-size: 18px;"> ¡Suscribite y enterate antes que nadie nuestras promociones!</p>
        </div>

        <div class="container pt-5 text-center">
            <form class="d-flex justify-content-center">
                <div class="form-group mr-3">
                    <label for="email" class="sr-only">Email</label>
                    <input type="password" class="form-cyber form-control" id="email" placeholder="email">
                </div>
                <a class="button_cyber" href="#">ENVIAR</a>
            </form>
        </div>      
      
        <div class="container pt-5 text-center">
            <div class="form-check form-check-inline">
                <p style="color: #FFFFFF;"> <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> 
                    Acepto que Unilever y todas sus marcas guarden mis datos personales y me envíen materiales informativos por cualquier medio, invitaciones para eventos y/o encuestas de sus marcas o productos.
                    Ud podrá ejercerel derecho de acceso y solicitar la rectificación, actualización o eliminación de los mismos en forma gratuita al  0800-888-6666 y/oconsumidor@sac-unilever.com.ar (ver Política de Privacidad)
                </p>
            </div>
        </div>    
    </section>

    <!---footer--->
    <footer class="footer container-fluid py-3 mb-5">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 text-xl-left text-lg-left text-md-center text-sm-center text-center">
                    <a class="nav-link d-inline" href="https://www.unilever.com.ar/legal.html">Aviso legal</a>
                    <span> | </span>
                    <a class="nav-link d-inline" href="https://www.unilevernotices.com/argentina/spanish/privacy-notice/notice.html">Politica de Privacidad</a>
                    <span> | </span>
                    <a class="nav-link d-inline" href="https://www.unilevernotices.com/argentina/spanish/cookie-notice/notice.html">Politica de Cookies</a>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 text-xl-right mt-0 text-lg-right mt-0 text-md-center mt-md-2 text-sm-center mt-sm-2 text-center mt-2">
                    <a class="nav-link d-inline" href="#">&copy;2019 Unilever</a>
                    <a href="#" target="https://www.instagram.com/shopunilever/?utm_source=shopunilever"> <img src="img/IG.svg" alt="instagram" width="50px;" ></a>
                    <a href="#" target="https://www.facebook.com/Shopunilever/?utm_source=shopunilever"> <img src="img/facebook.svg" alt="facebook" width="50px;" ></a>
                    <a href="#" target="https://twitter.com/Shop_Unilever/?utm_source=shopunilever"> <img src="img/twitter.svg" alt="twitter" width="50px;" ></a>
                </div>
            </div>
        </div>
    </footer>

    <!-- legales -->
    <div class="container mt-5">
        <div class="row">
            <div class="col-xl-10 col-lg-10 col-md-8 col-sm-12 col-xs-12">
                <p style="color: #868686; font-size: 12px; text-align: justify;">
                    El titular de los datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que se acredite un interés legítimo al efecto conforme lo establecido en el artículo 14, inciso 3 de la Ley Nº 25.326. La DIRECCION NACIONAL DE PROTECCION DE DATOS PERSONALES, Organo de Control de la Ley Nº 25.326, tiene la atribución de atender las denuncias y reclamos que se interpongan con relación al incumplimiento de las normas sobre protección de datos personales.
                    <br class="mb-3">
                    LA AGENCIA DE ACCESO A LA INFORMACIÓN PÚBLICA, en su carácter de Órgano de Control de la Ley N° 25.326, tiene la atribución de atender las denuncias y reclamos que interpongan quienes resulten afectados en sus derechos por incumplimiento de las normas vigentes en materia de protección de datos personales”. Se podrá ejercer el derecho al acceso llamando al 0800-888-6666 y/o por mail a consumidor@sac-unilever.com.ar
                    <br class="mb-3">
                    Promoción sin obligación de compra válida en Argentina con excepción de las provincias de Mendoza, Salta y Río Negro, desde 28/9/2019 hasta el 3/11/2019 inclusive. Para más información consulte en www.shopunilever.com/pre-registrocyber2019/legales donde obraran las B&C, premios y requisitos de participación. Organiza Unilever Argentina S.A.
                </p>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-12 col-xs-12 d-flex align-items-start justify-content-center">
                <img id="logounilever" class="" src="img/unileverlogo.svg" width="80px" alt="logo unilever">
            </div>
        </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="js/main.js"></script>
    <script src="js/owl.carousel.min.js"></script>    

  </body>
</html>