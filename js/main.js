$(document).ready(function () {
    console.log("ready!");
    $('.imghoover').hover(
    function () {
        $(this).children('.secondimg').removeClass("dnone").addClass("dblock");
        $(this).children('.firstimg').removeClass("dblock").addClass("dnone");
        $(this).children('.secondimg').css("transform","scale(1.1)");
        $(this).children('.secondimg').css("transition-duration","3s");
    },
    function(){
        $(this).children('.secondimg').removeClass("dblock").addClass("dnone");
        $(this).children('.firstimg').removeClass("dnone").addClass("dblock");
    });
    $('.imghooverdos').hover(
    function () {
            $(this).children('.firstimg').removeClass("dblock").addClass("dnone");
            $(this).children('.secondimg').removeClass("dnone").addClass("dblock"); 
            //$(this).children('.secondimg').css("transform", "scale(1.1");
          },
    function () {
        $(this).children('.secondimg').removeClass("dblock").addClass("dnone");
        $(this).children('.firstimg').removeClass("dnone").addClass("dblock");
    });


    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav: true,
        navText: ['<i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>', '<i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>'],
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true,
                loop: true
            },
            600:{
                items:2,
                nav:true,
                loop:true
            },
            1000: {
                items: 3,
                nav: true,
                loop: true
            },
            1360:{
                items:4,
                nav:true,
                loop:true
            }
        }
    })


    $(".open-modal").click(function(event) {
        var source = $(this).data("source");
        var category = $(this).data("category");
        var product = $(this).data("product");
        var eventCategory = "Click Compra";
        var eventAction = " " + category + " " + product;
        var action = source == "header" ? $(this).data("action") : category;
    
        console.log("Oferta " + eventCategory + eventAction);
        if (typeof utm != "undefined" && typeof utm.send != "undefined") {
          utm.send("Oferta " + eventCategory + eventAction);
        }
        event.stopImmediatePropagation();
    
        if (category != "Combo") {
          $("#modal-" + source).modal();
          ga("send", {
            hitType: "event",
            eventCategory: "Comprar " + category,
            eventAction: action,
            eventLabel: product
          });
        } else {
          category = source;
    
          var link = $(this).data("link");
    
          fbq("track", "Purchase");
    
          var retail = "Mercado Libre";
    
          eventAction = "Click " + retail;
          if (typeof utm != "undefined" && typeof utm.send != "undefined") {
            console.log("Retail " + eventAction);
            utm.send("Retail " + eventAction);
          }
          eventCategory = "Retail " + retail;
    
          ga("send", {
            hitType: "event",
            eventCategory: eventCategory,
            eventAction: category,
            eventLabel: product
          });
    
          /*$("body").prepend(
              '<iframe src="https://4914179.fls.doubleclick.net/activityi;src=4914179;type=sales;cat=krripiun;qty=[Quantity];cost=[Revenue];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;npa=;ord=[OrderID]?" width="1" height="1" frameborder="0" style="display:none"></iframe>'
              );*/
          /*if (typeof(Storage) !== "undefined") {
                                  localStorage.setItem("showPixel", true);
                    }*/
    
          gtag_report_conversion("https://shopunilever.com/" + campaignName);
    
          setTimeout(function() {
            window.open(link, "_blank");
          }, 1000);
        }
    
        event.stopImmediatePropagation();
      });

sources.forEach(function(source) {

    if (source.category != "Combo") {
      var divModal =
        `<div class="modal fade" id="modal-` +
        source.name +
        `" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                       <div class="modal-dialog" role="document">
                          <div class="modal-content">
                             <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                             </div>
                             <div class="modal-body">
                                <div class="row">`;
      if (source.name != "header") {
        divModal +=
          ` <div class="col-sm-6 producto-modal">
                                                            <img src="img/productos/` +
          source.category.toLocaleLowerCase() +
          `/` +
          source.name +
          `.jpg" class="img-fluid">
                                                            
                                                        </div>
                                                        <div class="col-sm-6 header-modal">`;
      } else {
        divModal += ` <div class="col-md-5 order-first">`;
      }
      divModal += `
                                        <h1>Hot Sale 2019</h1>
                                        <p>Elegí dónde vas a terminar tu compra
                                        </p>`;
      if (source.name == "header") {
        divModal += ` </div>
                            <div class="col-md-7 ">
                            <img src="img/packs.png" class="img-fluid">
                            </div>
                        </div>
                        <div>
                     <div class="row retails">`;
      } else {
        divModal += `<div class="retails modal-vertical"><div class="row">`;
      }

      source.retails.forEach(function(retail) {
        if (retail.link != "") {
          if (source.name == "header") {
            divModal += `<div class="col-lg-2 col-6">`;
          } else {
            divModal += `<div class="col-md-4 col-6">`;
          }
          divModal +=
            `<div class="card">
                            <a style="cursor:pointer"  data-source="` +
            source.name +
            `" data-retail="` +
            retail.name +
            `" data-link="` +
            retail.link +
            `">
            <img src="img/retails/` +
            (typeof retail.image != "undefined"
              ? retail.image +
                `"  
              class="mt-3 "`
              : retail.name +
                `.svg"  
              class="mt-3"`) +
            `
                >
                            <img src="img/retails/btnhover.jpg" class="img-top" >
                            </a>
                    </div>
                    </div>`;
        }
      });

      divModal += `
                                            
                                            
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>`;
      if (source.name != "header") {
        divModal += `
                        </div>
                        </div>`;
      }

      $("body").append(divModal);

      /*$(".btnretail").mouseenter(function() {
        var retail = $(this).data("retail");
        var source = $(this).data("source");
        $(this).hide();
        $("#btnhover-" + source + "-" + retail).show();
      });
      $(".btnhover").mouseleave(function() {
        var retail = $(this).data("retail");
        var source = $(this).data("source");
        $(this).hide();
        $("#btnretail-" + source + "-" + retail).show();
      });*/

      $("#modal-" + source.name + " a").click(function() {
        //fbq("track", "Purchase");

        var retail = $(this).data("retail");
        var category =
          source.name != "header" ? source.category : source.action;
        var product = source.title;
        if (source.name == "header") {
          product = "Header " + product;
        }
        console.log(source);
        var link = $(this).data("link");

        var eventAction = "Click " + retail;
        var eventLabel = campaignName + " " + source.name;
        if (typeof utm != "undefined" && typeof utm.send != "undefined") {
          utm.send("Retail " + eventAction);
        }
        console.log("Retail " + eventAction);
        var eventCategory = "Retail " + retail;

        ga("send", {
          hitType: "event",
          eventCategory: eventCategory,
          eventAction: category,
          eventLabel: product
        });

        gtag_report_conversion("https://shopunilever.com/" + campaignName);

        setTimeout(function() {
          window.open(link, "_blank");
        }, 1000);
      });
    } else {
    }
  });
});



var sources = [
    {
      category: "Alimentos",
      name: "knorr-cdo",
      title: "KNORR CD",
      retails: [
        { name: "farmacity", link: "" },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2984&utm_source=unilever&utm_campaign=unilever_cm2019_food&utm_medium=referral&utm_term=shop"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "knorr-cdo-verdura",
      title: "KNORR CDO VERDURA",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2984&utm_source=unilever&utm_campaign=unilever_cm2019_food&utm_medium=referral&utm_term=shop"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Cybermonday&utm_content=Web-shop-Alimentos#_atCategory=false&_atGrilla=true&_id=362407-shop#_atCategory=false&_atGrilla=true&_id=362407"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Shop-UL-Alimentos/busca/?fq=H:662&O=OrderByBestDiscountDESC?utm_source=ShopUL&utm_medium=web&utm_campaign=cm"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A3959&utm_source=Unilever&utm_medium=referral&utm_campaign=Cybermonday&utm_content=Web-shop-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Cybermonday&utm_content=Web-shop-Alimentos#_atCategory=false&_atGrilla=true&_id=213131"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/cyber-monday/alimentos.html?utm_source=shopunilever.com&utm_medium=referral&utm_content=unilever&utm_campaign=cyber-monday"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "hellmanns-barbacoa",
      title: "Hellmanns Barbacoa",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
  
    {
      category: "Alimentos",
      name: "knorr-pure-de-papas",
      title: "Knorr Pure de Papas",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link: "https://www.walmart.com.ar/busca?ft=knorr"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "knorr-pomarola",
      title: "Knorr Pomarola",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link: "https://www.walmart.com.ar/busca?ft=knorr"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "knorr-verdura",
      title: "Knorr Verdura",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link: "https://www.walmart.com.ar/busca?ft=knorr"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "knorr-quick-light",
      title: "Knorr Quick Light",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link: "https://www.walmart.com.ar/busca?ft=knorr"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "lipton-te-negro-forest-fruit",
      title: "Lipton Te Negro Forest Fruit",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        {
          name: "mercadolibre",
          link:
            "https://listado.mercadolibre.com.ar/alimentos-bebidas/comestibles/infusiones/te/_OrderId_PRICE*DESC_Tienda_lipton?utm_source=UNILEVER &utm_medium=REFERRAL&utm_campaign=Hot Sale-Mayo-2019-Alimentos-Lipton"
        },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "lipton-infusion-herbal-menta",
      title: "Lipton Infusion Herbal Menta",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        {
          name: "mercadolibre",
          link:
            "https://listado.mercadolibre.com.ar/alimentos-bebidas/comestibles/infusiones/te/_OrderId_PRICE*DESC_Tienda_lipton?utm_source=UNILEVER &utm_medium=REFERRAL&utm_campaign=Hot Sale-Mayo-2019-Alimentos-Lipton"
        },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "lipton-te-negro-yellow",
      title: "Lipton Te Negro Yellow",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        {
          name: "mercadolibre",
          link:
            "https://listado.mercadolibre.com.ar/alimentos-bebidas/comestibles/infusiones/te/_OrderId_PRICE*DESC_Tienda_lipton?utm_source=UNILEVER &utm_medium=REFERRAL&utm_campaign=Hot Sale-Mayo-2019-Alimentos-Lipton"
        },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "maizena-granola-manzana-canela",
      title: "Maizena Granola Manzana Canela",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "maizena-granola-pasas",
      title: "Maizena Granola Pasas",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "maizena-bizcochuelo-vainilla",
      title: "Maizena Bizcochuelo Vainilla",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "maizena-brownies",
      title: "Maizena Brownies",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Alimentos",
      name: "savora",
      title: "Savora",
      retails: [
        {
          name: "farmacity",
          link: ""
        },
        { name: "pigmento", link: "" },
        { name: "gpsfarma", link: "" },
        { name: "openfarma", link: "" },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2689&utm_source=unilever&utm_campaign=unile_hs2019_alimentos&utm_medium=referral&utm_term=landing_uni"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=156277"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Almacen-UL/?fq=H:386&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=almacen"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2143&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-Alimentos#_atCategory=false&_atGrilla=true&_id=262418"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/alimentos.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    /*END ALIMENTOS*/
    {
      category: "Ropa",
      name: "ala-jabon-liquido",
      title: "Ala Jabon Liquido",
      retails: [
        {
          name: "farmacity",
          link:
            "https://www.farmacity.com/busca/?fq=H:1894&utm_source=unilever&utm_medium=referral&utm_campaign=trafico-unilever-hot-sale_19_05&utm_content=sitio&utm_term=06-05-19"
        },
        {
          name: "pigmento",
          link:
            "https://perfumeriaspigmento.com.ar/hot-sale-2019/proveedor/unilever/promo-a?utm_source=unilever&utm_medium=referral&utm_campaign=cace-mega-unilever-a"
        },
        {
          name: "gpsfarma",
          link:
            "https://www.gpsfarma.com/hot-sale/unilever/cuidado-de-la-ropa.html?utm_source=Referral&utm_medium=Sitio_unilever&utm_campaign=hotsale_2019"
        },
        {
          name: "openfarma",
          link:
            "https://www.openfarma.com.ar/products?utf8=%E2%9C%93&keywords=skip"
        },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2688&utm_source=unilever&utm_campaign=unile_hs2019_cdoropa&utm_medium=referral&utm_term=landing_uni"
        },
        {
          name: "mercadolibre",
          link:
            "https://hogar.mercadolibre.com.ar/limpieza/cuidado-ropa/_OrderId_PRICE*DESC_Tienda_ala?utm_source=UNILEVER &utm_medium=REFERRAL&utm_campaign=Hot Sale-Mayo-2019-Cuidado-de-la-Ropa-Ala"
        },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa#_atCategory=false&_atGrilla=true&_id=109511"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Cuidado-dela-Ropa/?fq=H:346&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=Cuidadodelaropa"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2144&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa#_atCategory=false&_atGrilla=true&_id=109047"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/cuidado-de-la-ropa.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Ropa",
      name: "comfort-concentrado",
      title: "Comfort Concentrado",
      retails: [
        {
          name: "farmacity",
          link:
            "https://www.farmacity.com/busca/?fq=H:1894&utm_source=unilever&utm_medium=referral&utm_campaign=trafico-unilever-hot-sale_19_05&utm_content=sitio&utm_term=06-05-19"
        },
        {
          name: "pigmento",
          link:
            "https://perfumeriaspigmento.com.ar/hot-sale-2019/proveedor/unilever/promo-a?utm_source=unilever&utm_medium=referral&utm_campaign=cace-mega-unilever-a"
        },
        {
          name: "gpsfarma",
          link:
            "https://www.gpsfarma.com/hot-sale/unilever/cuidado-de-la-ropa.html?utm_source=Referral&utm_medium=Sitio_unilever&utm_campaign=hotsale_2019"
        },
        {
          name: "openfarma",
          link:
            "https://www.openfarma.com.ar/products?utf8=%E2%9C%93&keywords=skip"
        },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2688&utm_source=unilever&utm_campaign=unile_hs2019_cdoropa&utm_medium=referral&utm_term=landing_uni"
        },
        { name: "mercadolibre", link: "" },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa#_atCategory=false&_atGrilla=true&_id=109511"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Cuidado-dela-Ropa/?fq=H:346&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=Cuidadodelaropa"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2144&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa#_atCategory=false&_atGrilla=true&_id=109047"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/cuidado-de-la-ropa.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    },
    {
      category: "Ropa",
      name: "ala-matic-polvo",
      title: "Ala Matic Polvo",
      retails: [
        {
          name: "farmacity",
          link:
            "https://www.farmacity.com/busca/?fq=H:1894&utm_source=unilever&utm_medium=referral&utm_campaign=trafico-unilever-hot-sale_19_05&utm_content=sitio&utm_term=06-05-19"
        },
        {
          name: "pigmento",
          link:
            "https://perfumeriaspigmento.com.ar/hot-sale-2019/proveedor/unilever/promo-a?utm_source=unilever&utm_medium=referral&utm_campaign=cace-mega-unilever-a"
        },
        {
          name: "gpsfarma",
          link:
            "https://www.gpsfarma.com/hot-sale/unilever/cuidado-de-la-ropa.html?utm_source=Referral&utm_medium=Sitio_unilever&utm_campaign=hotsale_2019"
        },
        {
          name: "openfarma",
          link:
            "https://www.openfarma.com.ar/products?utf8=%E2%9C%93&keywords=skip"
        },
        {
          name: "walmart",
          link:
            "https://www.walmart.com.ar/busca?fq=H:2688&utm_source=unilever&utm_campaign=unile_hs2019_cdoropa&utm_medium=referral&utm_term=landing_uni"
        },
        {
          name: "mercadolibre",
          link:
            "https://hogar.mercadolibre.com.ar/limpieza/cuidado-ropa/_OrderId_PRICE*DESC_Tienda_ala?utm_source=UNILEVER &utm_medium=REFERRAL&utm_campaign=Hot Sale-Mayo-2019-Cuidado-de-la-Ropa-Ala"
        },
        { name: "linio", link: "" },
        {
          name: "vea",
          link:
            "https://www.veadigital.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa#_atCategory=false&_atGrilla=true&_id=109511"
        },
        {
          name: "dia",
          link:
            "https://diaonline.supermercadosdia.com.ar/Cuidado-dela-Ropa/?fq=H:346&O=OrderByBestDiscountDESC?utm_source=shopUL&utm_campaign=HotSale&utm_medium=Cuidadodelaropa"
        },
        {
          name: "jumbo",
          link:
            "https://www.jumbo.com.ar/busca?fq=H%3A2144&utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa"
        },
        {
          name: "disco",
          link:
            "https://www.disco.com.ar/Comprar/Home.aspx?utm_source=Unilever&utm_medium=referral&utm_campaign=Web&utm_content=HotSale-CuidadoRopa#_atCategory=false&_atGrilla=true&_id=109047"
        },
        {
          name: "carrefour",
          link:
            "https://supermercado.carrefour.com.ar/unilever/cuidado-de-la-ropa.html?utm_source=webunilever&utm_medium=referral&utm_content=categoria&utm_campaign=hot-sale"
        }
      ]
    }
]